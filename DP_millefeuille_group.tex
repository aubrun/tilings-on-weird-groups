\documentclass[letterpaper]{article}
\usepackage[english]{babel}

\usepackage{authblk}%for authors' affiliations

\usepackage{amsmath,amsthm}
\usepackage{amssymb}
\usepackage{float}
\usepackage{enumitem}
\usepackage{empheq}% left brace for block of equations

\usepackage[colorlinks = true,
            linkcolor = vert,
            urlcolor  = vert,
            citecolor = vert,
            anchorcolor = vert]{hyperref}

%centrer une figure qui dépasse des marges
\makeatletter

\newskip\@bigflushglue \@bigflushglue = -100pt plus 1fil

\def\bigcenter{\trivlist \bigcentering\item\relax}
\def\bigcentering{\let\\\@centercr\rightskip\@bigflushglue%
\leftskip\@bigflushglue
\parindent\z@\parfillskip\z@skip}
\def\endbigcenter{\endtrivlist}

\makeatother

\usepackage{graphics,graphicx,xcolor}
\usepackage{standalone}
\usepackage{tikz}
\usetikzlibrary{calc,shapes.arrows,decorations.pathmorphing,decorations.pathreplacing,decorations.markings,arrows,positioning,patterns,shapes.multipart}

\definecolor{rouge}{RGB}{255,77,77}
\definecolor{vert}{RGB}{0,178,102}
\definecolor{jaune}{RGB}{255,255,0}
\definecolor{violet}{RGB}{208,32,144}
\definecolor{orange}{RGB}{255,140,0}
\definecolor{bleu}{RGB}{0,0,205}

\theoremstyle{plain}

\newtheorem*{theorem*}{Theorem}
\newtheorem{theorem}{Theorem}%[section]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{conjecture}[theorem]{Conjecture}
\newtheorem*{questions*}{Questions}
\newtheorem{example}[theorem]{Example}
\newtheorem{remark}[theorem]{Remark}


\def\A{\mathcal{A}}
\def\B{\mathcal{B}}
\def\F{\mathcal{F}}
\def\N{\mathbb{N}}
\def\Q{\mathbb{Q}}
\def\R{\mathbb{R}}
\def\Z{\mathbb{Z}}

\newcommand{\seed}{\vbox to 8pt{\hbox{
\begin{tikzpicture}[scale=0.3]
\draw[fill=black!15] (0,0) rectangle (1,1);
\node[scale=0.5,color=black] at (0.5,0.5) {\textbullet};
\end{tikzpicture}
}}}

\newcommand{\marked}{\vbox to 8pt{\hbox{
\begin{tikzpicture}[scale=0.3]
\draw[fill=black!15] (0,0) rectangle (1,1);
\end{tikzpicture}
}}}

\newcommand{\todo}[1]{\textcolor{bleu}{\textsc{TODO: #1}}}
\newcommand{\define}[1]{\emph{#1}}


\title{About the Domino problem on\\ \emph{millefeuille} groups.}
\date{}
%\author{Nathalie Aubrun, Julien Esnay and  Benjamin Hellouin}

\author[1]{Nathalie Aubrun}
\author[2]{Julien Esnay}
\author[3]{Benjamin Hellouin}
\affil[1]{CNRS, Universit\'e Paris-Saclay, LISN, 91400 Orsay, France}
\affil[2]{IMT, Universit\'e Paul Sabatier, 31062 Toulouse, France}
\affil[3]{Universit\'e Paris-Saclay, LISN, 91400 Orsay, France}
\setcounter{Maxaffil}{0}
\renewcommand\Affilfont{\small}

\begin{document}
 
\maketitle 
 
\begin{abstract}
In this article we study subshifts on the \emph{millefeuille} groups $M_{m,n}\langle a,b\mid ab^m=ba^n \rangle$. In particular we investigate two classical problems for subshifts of finite type (SFT) defined on a finitely generated group: the Domino problem and the problem of the existence of aperiodic SFT. 
\end{abstract}

%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%
\section*{Introduction}
\label{section.introduction}


Usefull links \href{https://math.stackexchange.com/questions/3192647/understanding-hnn-extensions-intuition-examples-exercises}{[here]} (about HNN extensions and semi-direct product) and \href{https://math.stackexchange.com/questions/3941422/do-you-know-this-finitely-presented-group-on-two-generators/3941463#3941463}{[here]} (where the case of $M_{2,2}$ is discussed).

%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%
\section{Basics of combinatorial group theory}
\label{section.basics_comb_group_th}


\subsection{HNN extensions and Britton's lemma}
\label{subsection.HNN_Britton}

Let $G=\langle S\mid R \rangle$, $H,K$ two subgroups of $G$,  and $\alpha:H\to K$ a isomorphism. Let $t$ be a new letter not in $S$, then the \define{HNN extension relative to $\alpha$} is
\[
G_{*\alpha}:=\langle S,t \mid R, tht^{-1}=\alpha(h)\text{ for every }h\in H\rangle.
\]

The letter $t$ is called the \define{stable letter} of $G_{*\alpha}$, and the group $G$ is the \define{base group} of $G_{*\alpha}$.

\medskip

The HNN extension is called \define{ascending} if $\alpha:G\to G$ is an injective homomorphism, and \define{strictly ascending} if moreover $\alpha:G\to G$ is not onto.
%other name for ascending HNN extension : mapping torus

\begin{example}
Consider Baumslag-Solitar groups $BS(m,n)=\langle a,t\mid ta^nt^{-1}=a^m\rangle$ with $m,n\in\N$. Then $BS(m,n)$ is an HNN extension with $H=n\Z$, $K=m\Z$ and $\alpha(k\cdot n)=k\cdot m$ for every $k\in\Z$. Thus
\begin{itemize}
\item $BS(1,1)$ is a non strictly ascending HNN extension;
\item $BS(1,n)$ for $n>2$ are strictly ascending HNN extensions;
\item $BS(m,n)$ for $m,n>2$ are non ascending HNN extensions.
\end{itemize}
\end{example}

\begin{lemma}{(Britton's lemma)}
Let $w\in G_{*\alpha}$ such that
\[
w=g_0t^{\epsilon_1}g_1t^{\epsilon_2}\dots g_{n-1}t^{\epsilon_n}g_n
\]
with $g_i\in G$ and $\epsilon_i= \pm 1$. If one of the following hold
\begin{itemize}
\item $n=0$ and $g_0\neq 1_G$,
\item $n>0$ and $w$ contains no subword of the form $tht^{-1}$ with $h\in H$ or $t^{-1}kt$ with $k\in K$,
\end{itemize}
then $w\neq 1_{G_{*\alpha}}$.
\end{lemma}

In the case of an ascending HNN extension with $\alpha:G\to G$ an isomorphism, the Britton normal form simplifies.

\begin{lemma}{(Britton's lemma for ascending HNN extension)}
\label{lemma.Britton_ascending}
Assume $\alpha:G\to G$ is an isomorphism. Then any group element $g\in G_{*\alpha}$ can be written as 
\[
w=g_0t^{\epsilon}g_1t^{\epsilon}\dots g_{n-1}t^{\epsilon}g_n
\]
with $g_i\in G$ and $\epsilon= \pm 1$.  Moreover if $n>0$ or if $n=0$ and $g_0\neq 1_G$, then $w\neq 1_{G_{*\alpha}}$.
\end{lemma}

\subsection{One relator groups}
\label{subsection.one_relator_groups}

\begin{theorem}[\cite{Magnus1932}]
\todo{find a nice statement}
\end{theorem}



%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%
\section{Symbolic dynamics on finitely generated groups}
\label{section.SD_fg_groups}

\subsection{Generalities}
\label{subsection.SD_generalities}

\subsection{Aperiodic subshifts}
\label{subsection.aperiodic}

Every countable group admits an aperiodic subshift~\cite{GaoJacksonSeward2009,AubrunBarbieriThomasse2018}, but aperiocity reveals much more challenging to obtain inside SFTs.

Finitely generated groups admitting strongly aperiodic SFTs are $\Z^2$\cite{Berger1964,Robinson1971} and $\Z^d$ for~$d\geq3$~\cite{CulikKari1996},  fundamental groups of oriented surfaces~\cite{CohenGoodmanStrauss2017}, hyperbolic groups~\cite{CohenGSRieck2017}, amenable Baumslag-Solitar groups~\cite{EsnayMoutot2020,AubrunSchraudner2020}

\subsection{The Domino problem}
\label{subsection.DP}


%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%
\section{The \emph{millefeuille} groups}
\label{section.millefeuille_groups}

Inspired by the work on Baumslag-Solitar groups~\cite{AubrunKari2013,AubrunKari2021}, we investigate groups 
\[
 M_{m,n}:=\langle a,b\mid ab^m=ba^n\rangle.
\]
We first study the group $M_{2,2}$ and write it as a semi-direct product (Section~\ref{subsection.Freiheitsatz_to_M22}), and then describe the Cayley graph of $M_{2,2}$ according to this new presentation (Section~\ref{subsection.Cayley_graphs_M22}).

\subsection{Magnus's \emph{Freiheitsatz} applied to millefeuille group~$M_{2,n}$}
\label{subsection.Freiheitsatz_to_M2n}

In this section we focus on the \emph{millefeuille} group $M_{2,n}:=\langle a,b\mid ab^2=ba^n\rangle$ for $n\geq2$. Since $M_{2,n}$ -- like all other \emph{millefeuille} groups $M_{m,n}$ -- is a one-relator group, we can apply Magnus's \emph{Freiheitsatz} to better understand its structure.

In our case, Magnus's \emph{Freiheitsatz} gives 

\begin{proposition}
\label{proposition.M2n_semi_direct_product}
The \emph{millefeuille} group $M_{2,n}$ can be written as a semi-direct product $\langle x,y\rangle \rtimes_f \mathbb{Z}$.
\end{proposition}

\begin{proof}
We denote $R:=ab^2a^{-n}b^{-1}$ the relator defining $M_{2,n}$. Since the abelianized presentation of $M_{2,n}$ is $\langle a,b\mid ba^{-n+1}=1\rangle$, we change the generating system $\{a,b\}$ for $\{x,t\}$ where $x=ba^{-n+1}$ and $t=a^{-1}$. The old generators can be expressed as $a=t^{-1}$ and $b=xt^{-n+1}$, and the relator $R$ becomes
\begin{align*}
R &= ab^2a^{-n}b^{-1}\\
 &= t^{-1}\left(xt^{-n+1}\right)^2 t^n \left(xt^{-n+1}\right)^{-1}\\
R &= (t^2xt^{-2})(txt^{-1})x^{-1}.
\end{align*}
Denote $y=txt^{-1}$, so that $R=tyt^{-1}yx^{-1}$. Thus by Magnus's \emph{Freiheitsatz}, since the relator $R$ cannot be written as a word in $x$ and $y$,  we know that $\{x,y\}$ is a basis for a free subgroup of $M_{2,2}$. We thus have a new presentation for $M_{2,2}$
\[
 M_{2,2}=\langle x,y,t\mid txt^{-1}=y,tyt^{-1}=xy^{-1}\rangle,
\]
in which one recognizes an HNN extension, of the free group with two generators $\mathbb{F}_2$, or said otherwise the semi-direct product
\[
 M_{2,2}=\langle x,y\rangle \rtimes_f \mathbb{Z},
\]
where $f:\mathbb{Z}\to Aut(\mathbb{F}_2)$ is given by powers $f(k)=\varphi^k$ of the automorphism $\varphi:\mathbb{F}_2 \to \mathbb{F}_2$ defined by $\varphi(x)=y$ and $\varphi(y)=xy^{-1}$.
\end{proof}


\subsection{A normal form for $M_{2,2}$}
\label{subsection.normal_form_M22}

Applying Lemma~\ref{lemma.Britton_ascending} to $M_{2,2}$ gives that any non trivial group element $g\in M_{2,2}$ can be written as
\[
w=g_0t^{\epsilon}g_1t^{\epsilon}\dots g_{n-1}t^{\epsilon}g_n
\]
with $g_i\in \langle x,y\rangle$, $\epsilon= \pm 1$ and either $n>0$ or $n=0$ and $g_0\neq 1_{\langle x,y\rangle}$.


\subsection{Cayley graphs of $M_{2,2}$}
\label{subsection.Cayley_graphs_M22}

With the standard presentation $M_{2,2}:=\langle a,b\mid ab^2=ba^2\rangle$, the Cayley graph is pictured on Figure~\ref{figure.Cayley_graph_M22_standard}.

    \begin{figure}[!ht]
        \centering
        \include{figures/Cayley_graph_M22_standard}
        \caption{A portion of the right Cayley graph of the \emph{millefeuille} group $M_{2,2}$ with generating set  $\{a,b\}$.}
            \label{figure.Cayley_graph_M22_standard}
    \end{figure}

For the purpose of this paper, the presentation exhibited in Section~\ref{subsection.Freiheitsatz_to_M22} is more appropriate. Remind that $M_{2,2}=\langle x,y,t\mid txt^{-1}=y,tyt^{-1}=xy^{-1}\rangle$.

    \begin{figure}[!ht]
        \centering
        \include{figures/basic_blocks_M22}
        \caption{The two basic blocks to build the Cayely graph of the \emph{millefeuille} group $M_{2,2}$.}
            \label{figure.basic_blocks_M22}
    \end{figure}

Build up thanks to the two basic blocks pictured on Figure~\ref{figure.basic_blocks_M22}, the new Cayley graph is depicted on Figures~\ref{figure.Cayley_graph_M22_new}. 

    \begin{figure}[!ht]
        \centering
        \includegraphics[width=0.8\linewidth]{figures/Cayley_graph_M22_new.JPG}
        \caption{The  Cayely graph of the \emph{millefeuille} group $M_{2,2}$ with generating set $\{x,y,t\}$.}
            \label{figure.Cayley_graph_M22_new}
    \end{figure}

    

\subsection{Orbit graphs in $M_{2,2}$}

The writting of $M_{2,2}$ as an HNN extension suggests that orbit graphs of the substitution $\sigma:\{1,2,3,4\} \to \{1,2,3,4\}$ may be found, inside the Cayley graph,  with $\sigma(1)=3$, $\sigma(2)=4$, $\sigma(3)=14$ and $\sigma(4)=32$. To formalize this intuition, we define an SFT $X\subset \left\{1,2,3,4\right\}^{M_{2,2}}$ where configurations are built up from the allowed patterns of Figure~\ref{figure.Rules_SFT_X}.

    \begin{figure}[!ht]
        \centering
        \includegraphics[width=0.8\linewidth]{figures/Rules_SFT_X.JPG}
        \caption{The  allowed patterns that define the SFT $X$.}
            \label{figure.Rules_SFT_X}
    \end{figure}
    
\begin{proposition}
The SFT $X$ is non-empty.
\end{proposition}

\begin{proof}

\end{proof}

  
\begin{proposition}
The SFT $X$ is strongly aperiodic ????
\end{proposition}

\begin{proof}

\end{proof}  
  
%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%
\section{Seeded Domino problem }
\label{section.3}  
  
In its seeded version,  the Seeded Domino problem (SDP) on a finitely generated group $G$ takes as input an SFT $X\subset A^G$ and a letter $a\in A$, and outputs \texttt{Yes} if and only if there exists a configuration $x\in X$ such that $x_{1_G}=a$.

\bigskip

\begin{theorem}\label{theorem.undecidable_SDP_HNN-extension}
Let $\widetilde{G}$ be an HHN-extension $\widetilde{G}=G_{*\alpha}$ where $\alpha:H\to K$ is an isomorphism with $H$ infinite and finitely presented. Then the SDP is undecidable on $G$.
\end{theorem}

\begin{proof}[Sketch of the proof]

\begin{itemize}
\item Same use of Seward's result ($Z$ acts translation like on every infinite group) as in Jeandel's proof that DP is undecidable on $G_1\times G_2$ with $G_1$ and $G_2$ infinite: existence of a non-empty SFT on $G$ such that all its configuration partition $G$ into copies of $\Z$.
\item Thanks to the seed kill all copies of $\Z$ in $G$ but one that will serve as the tape at time $0$./The seed and local rules to kill all bi-infinite paths in $G$ but the one containing the identity $1_{\widetilde{G}}=1_G$
\item Use the HNN-extension local rules $t^{-1}ht=\alpha(h)$ to construct iteratively tapes at time $k$ for every $k\in\N$ inside a seeded SFT on $\widetilde{G}$
\item Add you favourite TM and we are done! Undecidability of the seeded Domino problem
\end{itemize}

\end{proof}

We first recall a theorem by Seward, and follow Jeandel's interpretation in term of SFTs.

\begin{theorem}{\cite{Seward2014}}
The group $\Z$ acts translation like on every infinite finitely generated group $G$.
\end{theorem}
  
\begin{corollary}{\cite{Jeandel2015translation}}
There exists a finite generating set $S$ for $G$ such that the Cayley graph $Cay(G,S)$ can be partitioned into disjoint bi-infinite paths.
\end{corollary}  
  
\begin{corollary}{\cite{Jeandel2015translation}}
There exists a map $next:G\to G$ which states which element $next(g)$ in $G$ follows $g$ in the partition into bi-infinite paths. Moreover this function $next$ satisfies that
\begin{enumerate}
\item $next$ is onto and one-to-one, we call $pred$ its inverse $pred:=next^{-1}$
\item $next(h)$ is a neighbor of $h$ in $Cay(G,S)$, in other words for every $g\in G$ one has $g^{-1} next(g)\in S$
\item each path is infinite: for every $n>0$ and every $g\in G$ one has $next^n()\neq g$.
\end{enumerate}
\end{corollary}    

From this formulation Jeandel deduces that the following SFT is non-empty

\begin{align*}
X_{G,S}&=
 \begin{aligned}[t]
  \Big\{
  &x\in A^G \colon \forall s\in S, \left[ (x_{g})_1=s \Rightarrow (x_{gs})_2=s^{-1}\right]\\
 & \wedge \left[ (x_{g})_2=s \Rightarrow (x_{gs})_1=s^{-1}\right]
  \Big\},
 \end{aligned}
\end{align*}
where $A$ denotes the product alphabet $\left(S\times S\right)$.

The same local rules also give an SFT on $\widetilde{G}$ this time, called $X_{\widetilde{G},S}$.  A configuration $x$ of $X_{\widetilde{G},S}$ is such that for every $k\in\Z$,  the coset $t^k\cdot G$ is partitioned into bi-infinite disjoint paths.

\todo{définir proprement le Seeded Domino problem}

We add a seed, denoted $\seed$,  so that we now consider the seeded SFT $\dot{X}_{\widetilde{G},S}\subset \left(A\cup A\times\left\{\seed\right\}\right)^{\widetilde{G}}$ where all configurations $x\in \dot{X}_{\widetilde{G},S}$ satisfy $x_g \in A\times\left\{\seed\right\}$ if and only if $g=1_{\widetilde{G}}$, i.e. the seed appears only at the identity.

\medskip

\todo{on peut par exemple choisir les plus petites pour l'ordre lexicographique ? choisir un arbre couvrant des géodésiques ?}
We arbitrarily fix a unique geodesic between any two elements $g,g'\in G$, so that these choices are globally compatible on $G$ (i.e. if the chosen geodesic from $g$ to $g'$ goes through $g''$, then it goes through the chosen geodesics from $g$ to $g''$ and from $g$ to $g'$).

\medskip

Thanks to the seed, we modify $\dot{X}_{\widetilde{G},S}$ into $\dot{Y}_{\widetilde{G},S}$ by changing the alphabet to 
\[
B:=A\cup A\times\left\{\seed\right\}\cup A\times\left\{\marked\right\}
\]

and by adding the following local rules:
\begin{enumerate}
\item if $x_g=\left(., \seed\right)$ then $x_{g\cdot next(g)}=\left(., \seed\right)$ and $x_{g\cdot prev(g)}=\left(., \seed\right)$
\item if $x_g=\left(., \seed\right)$ or $x_g=\left(., \marked\right)$ then $x_{g\cdot t}=\left(., \marked\right)$
\item 
\item 

\item the pattern $x_g=\left((n,p), \marked\right)$, $x_{g'=}\left((n',p'), \marked\right)$ and $x_{g''}=\left((n'',p''), \marked\right)$ is forbidden if $g\cdot n=g'\cdot n'$
\end{enumerate}

%Informally this SFT preceedes a more complex one, where infinte paths defined in $X$ will stand for codings of bi-infinite words inside an arrangement of orbit graphs of a well-chosen substitution. 
 
\begin{proof}[Proof of Theorem~\ref{theorem.undecidable_SDP_HNN-extension}]
 
\end{proof}
  
%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%
\section{Domino problem for $G=H_{*\alpha}$ with $H$ infinite and finitely generated}
\label{section.4}  
  
Let $G$ be an HHN-extension $G=H_{*\alpha}$  with $H$ infinite and finitely presented.

 
%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%
\section{Toward a proof of the Domino conjecture for one-relator groups?}
\label{section.5}    
  
A one-relator group $\langle S\mid r\rangle$ is virtually free iff $r$ is a power of a primitive element in $S$ (see \href{https://mathoverflow.net/questions/323660/infinitely-presented-group-where-every-finite-sub-presentation-is-virtually-free}{this topic}).
  
If $G$ is a group, an element $g\in G$ is primitive if for every $h\in G$ such that $h^k=g$ for $k>0$, then  $k=1$ and $h=g$ (see \href{https://math.stackexchange.com/questions/1904623/existence-of-primitive-element-in-a-non-abelian-group}{this topic}).  So $r=g^\ell$ for some $g$ primitive and $\ell>0$.

\todo{Magnus-Moldovanskii hierarchy : ends on a  $\Z_{/m\Z}*\mathbb{F}_n$ for some $m,n\in \N$}
   
\todo{des trucs intéressants \href{http://theses.gla.ac.uk/5210/}{ici}}   
   
\todo{à creuser ? dans cet \href{https://www.tandfonline.com/doi/abs/10.1080/00927877708822215}{article} on montre que tout groupe à un relateur possède un sous-groupe d'indice fini sans torsion}   
   
\begin{itemize}
\item understand how to write the recurrence : what is critical in the writing as an HNN extension? distinguish the cases finite index/infinite index of $H$ in G?
\item which orbit graph to look for? is the generating set $S$ given by Jeandel's interpretation of Seward restult the good alphabet to define a substitution?
\end{itemize}  
  
\section*{Acknowledgments}

We are grateful to Yves Cornulier for pointing out the Math.stackexchange \href{https://math.stackexchange.com/questions/3941422/do-you-know-this-finitely-presented-group-on-two-generators/3941463#3941463}{discussion} that lead to Proposition~\ref{proposition.M22_semi_direct_product}.

This work was partially supported by the ANR project CoCoGro (ANR-16-CE40-0005).

% %\small
\bibliographystyle{alpha}
\bibliography{biblio}
 
\end{document}

